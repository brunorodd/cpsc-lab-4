/*
 File:        mazesolver.h
 Purpose:     Contains constants, prototypes, globals
 Author:      Bruno Rodriguez and Mark Howarth
 Student #s:  19024173 and 22629737
 CS Accounts: u9n1b and j3k1b
 Date:        Nov 10th, 2017
 */


#pragma once

/* Global variables used to store all correct paths to an exit.  We initialize out
   global variables inside the mazesolver.c source file */
char ** paths;
int     paths_found;

/* Preprocessor directives to define macros */
#define MAZE1     "maze1.txt"
#define MAZE2     "maze2.txt"
#define MAZE3     "maze3.txt"
#define MAZE119   "maze119.txt"
#define MAZE3877  "maze3877.txt"
#define MAZE_WALL '*'
#define VISITED   'Y'
#define UNVISITED 'N'
#define BUFFER    128

/* Structure used as the cell for the maze representation */
typedef struct maze_cell {
    char character;
    char visited;
} maze_cell; /* We're using a little trick here.  Instead of writing struct maze_cell everywhere
                in our code, we're writing maze_cell.  We can do this because at the end of our
                struct maze_cell definition, we've tacked on the short form of the struct name.
				When we do this, the C standard lets us use maze_cell instead of struct maze_cell.  */

/* Function prototypes */
void         print_generated_paths ( );
maze_cell ** parse_maze            ( FILE * maze_file, int dimension );
int          get_maze_dimension    ( FILE * maze_file );
void         generate_all_paths    ( maze_cell ** maze, int dimension, int row, int column, char * path );
int          path_cost             ( char * path_string );
void		 display_shortest_path ( );
void		 display_cheapest_path ( );
